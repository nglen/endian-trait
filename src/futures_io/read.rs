use crate::{BytesConv, Endian};
use core::convert::{AsMut, AsRef};
use core::pin::Pin;
use futures::{
    ready,
    task::{Context, Poll},
    AsyncRead, Future,
};
use std::io;

#[derive(Debug)]
#[must_use = "futures do nothing unless you `.await` or poll them"]
pub struct ReadBytes<'a, R, C, E>
where
    R: ?Sized,
    C: BytesConv,
{
    reader: &'a mut R,
    bytes: C::Bytes,
    count: usize,
    endian: E,
}

impl<'a, C, R, E> ReadBytes<'a, R, C, E>
where
    R: ?Sized,
    C: BytesConv,
    C::Bytes: Unpin,
    E: Endian,
{
    fn new(reader: &'a mut R, endian: E) -> Self {
        Self {
            reader,
            endian,
            count: 0,
            bytes: Default::default(),
        }
    }
}

impl<C, R, E> Future for ReadBytes<'_, R, C, E>
where
    R: ?Sized + AsyncRead + Unpin,
    C: BytesConv,
    C::Bytes: Unpin,
    E: Endian,
{
    type Output = io::Result<C>;

    // TODO Is there a way to reuse read_exact instead of reimplementing it?
    // The problem is constructing the self referential struct.
    fn poll(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        let this = &mut *self;

        let len = this.bytes.as_ref().len();
        while this.count < len {
            let n = ready!(Pin::new(&mut this.reader).poll_read(cx, this.bytes.as_mut()))?;
            this.count += n;
            if n == 0 {
                return Poll::Ready(Err(io::ErrorKind::UnexpectedEof.into()));
            }
        }
        let res = self.endian.from_bytes(self.bytes);
        Poll::Ready(Ok(res))
    }
}

pub trait ReadBytesExt: AsyncRead {
    fn read_bytes<C, E>(&mut self, e: E) -> ReadBytes<'_, Self, C, E>
    where
        Self: Unpin,
        C: BytesConv,
        C::Bytes: Unpin,
        E: Endian,
    {
        ReadBytes::new(self, e)
    }
}

impl<R: AsyncRead> ReadBytesExt for R {}
