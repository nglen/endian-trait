use crate::BytesConv;

pub trait Endian: Copy + Unpin {
    fn from_bytes<C: BytesConv>(self, bytes: C::Bytes) -> C;
    fn to_bytes<C: BytesConv>(x: C) -> C::Bytes;
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub struct BE;
#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub struct LE;
#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub struct NE;

impl Endian for BE {
    fn from_bytes<C: BytesConv>(self, bytes: C::Bytes) -> C {
        C::from_be_bytes(bytes)
    }
    fn to_bytes<C: BytesConv>(x: C) -> C::Bytes {
        x.to_be_bytes()
    }
}

impl Endian for LE {
    fn from_bytes<C: BytesConv>(self, bytes: C::Bytes) -> C {
        C::from_le_bytes(bytes)
    }
    fn to_bytes<C: BytesConv>(x: C) -> C::Bytes {
        x.to_le_bytes()
    }
}

impl Endian for NE {
    fn from_bytes<C: BytesConv>(self, bytes: C::Bytes) -> C {
        C::from_ne_bytes(bytes)
    }
    fn to_bytes<C: BytesConv>(x: C) -> C::Bytes {
        x.to_ne_bytes()
    }
}
