pub trait BytesConv {
    type Bytes: Default + AsRef<[u8]> + AsMut<[u8]> + Copy;

    fn from_be_bytes(bytes: Self::Bytes) -> Self;
    fn from_le_bytes(bytes: Self::Bytes) -> Self;
    fn from_ne_bytes(bytes: Self::Bytes) -> Self;

    fn to_be_bytes(self) -> Self::Bytes;
    fn to_le_bytes(self) -> Self::Bytes;
    fn to_ne_bytes(self) -> Self::Bytes;
}

macro_rules! impl_bytes_conv {
    ($($type:ty = $len:literal),+) => {
        $(impl BytesConv for $type {
            type Bytes = [u8; $len];

            fn from_be_bytes(bytes: Self::Bytes) -> Self {
                <$type>::from_be_bytes(bytes)
            }
            fn from_le_bytes(bytes: Self::Bytes) -> Self {
                <$type>::from_le_bytes(bytes)
            }
            fn from_ne_bytes(bytes: Self::Bytes) -> Self {
                <$type>::from_ne_bytes(bytes)
            }

            fn to_be_bytes(self) -> Self::Bytes {
                self.to_be_bytes()
            }
            fn to_le_bytes(self) -> Self::Bytes {
                self.to_le_bytes()
            }
            fn to_ne_bytes(self) -> Self::Bytes {
                self.to_ne_bytes()
            }
        })+
    };
}

impl_bytes_conv!(u8 = 1, u16 = 2, u32 = 4, u64 = 8, u128 = 16);
impl_bytes_conv!(i8 = 1, i16 = 2, i32 = 4, i64 = 8, i128 = 16);
impl_bytes_conv!(f32 = 4, f64 = 8);
