use crate::{BytesConv, Endian};
use std::io::{Read, Result, Write};

pub trait ReadBytesExt {
    fn read_bytes<C: BytesConv>(&mut self, e: impl Endian) -> Result<C>;
}

pub trait WriteBytesExt {
    fn write_bytes<E: Endian>(&mut self, v: impl BytesConv) -> Result<()>;
}

impl<T: Read> ReadBytesExt for T {
    fn read_bytes<C: BytesConv>(&mut self, e: impl Endian) -> Result<C> {
        let mut buf = C::Bytes::default();
        self.read_exact(buf.as_mut())?;
        Ok(e.from_bytes(buf))
    }
}

impl<T: Write> WriteBytesExt for T {
    fn write_bytes<E: Endian>(&mut self, v: impl BytesConv) -> Result<()> {
        let buf = E::to_bytes(v);
        self.write_all(buf.as_ref())
    }
}
